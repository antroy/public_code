function EntryCtrl($scope) {
    $scope.entries = [
        {id: 1, date: "13/01/2013", payee: "Sainsburys", category: "Day to Day/Groceries", outflow: 34.22},
        {id: 2, date: "15/01/2013", payee: "Shell", category: "Transport/Fuel", outflow: 67.98},
        {id: 3, date: "19/01/2013", payee: "Primark", category: "Day to Day/Clothing", outflow: 29.66},
    ];
    
    $scope.addEntry = function(){
        $scope.entries.push({
            id: $scope.entries.length + 1, 
            date: $scope.newDate , 
            payee: $scope.newPayee, 
            category: $scope.newCategory, 
            outflow: $scope.newOutflow,
            inflow: $scope.newInflow
        });
    }
}

