YNAB.EntryController = Ember.ArrayController.extend({
  createEntry: function () {
    var date = this.get('newDate');
    var payee = this.get('newPayee');
    var category = this.get('newCategory');
    var memo = this.get('newMemo');
    var outflow = this.get('newOutflow');
    var inflow = this.get('newInflow');

    var entry = YNAB.Entry.createRecord({
        date: date ,
        payee: payee ,
        category: category,
        memo: memo ,
        outflow: outflow ,
        inflow: inflow
    });

    this.set('newDate', '');
    this.set('newPayee', '');
    this.set('newCategory', '');
    this.set('newMemo', '');
    this.set('newOutflow', '');
    this.set('newInflow', '');

    entry.save();
  }
});
