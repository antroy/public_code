YNAB.Router.map(function () {
      this.resource('entry', { path: '/' });
});

YNAB.EntryRoute = Ember.Route.extend({
    model: function () {
        return YNAB.Entry.find();
    }
});
