YNAB.Entry = DS.Model.extend({
    id: DS.attr("string"),
    checked: DS.attr("string"),
    date: DS.attr("date"),
    payee: DS.attr("string"),
    category: DS.attr("string"),
    memo: DS.attr("string"),
    outflow: DS.attr("number"),
    inflow: DS.attr("number")
});


YNAB.Entry.FIXTURES = [
    {id: 1, date: "13/01/2013", payee: "Sainsburys", category: "Day to Day/Groceries", outflow: 34.22},
    {id: 2, date: "15/01/2013", payee: "Shell", category: "Transport/Fuel", outflow: 67.98},
    {id: 3, date: "19/01/2013", payee: "Primark", category: "Day to Day/Clothing", outflow: 29.66},
];
